//
//  VoyakaNetTests.swift
//  VoyakaNet Tests
//
//  Created by Petr Reznikov on 18.03.17.
//  Copyright © 2017 Suvorov Startup Community. All rights reserved.
//

import XCTest
@testable import VoyakaNet

class VoyakaNetTests: XCTestCase {
    //MARK: Voyaka Class Tests
    func testVoyakaInitSucceeds() {
        // Highest positive rating
        let voyaka = Voyaka.init(name: "Positive", photo: nil, rating: 5)
        XCTAssertEqual(voyaka.rating, 5)
    }
}
