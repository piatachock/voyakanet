//
//  Voyaka.swift
//  VoyakaNet
//
//  Created by Petr Reznikov on 19.03.17.
//  Copyright © 2017 Suvorov Startup Community. All rights reserved.
//

import UIKit

/*
class Voyaka {
    //MARK: Properties
    var name: String
    var photo: UIImage?
    var rating: UInt8
    init(name: String, photo: UIImage?, rating: UInt8) {
        self.name = name
        self.photo = photo
        self.rating = 0
    }
}
*/

struct Voyaka {
    let name: String
    let photo: UIImage?
    var rating: UInt8
}
