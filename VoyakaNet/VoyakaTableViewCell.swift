//
//  VoyakaTableViewCell.swift
//  VoyakaNet
//
//  Created by Petr Reznikov on 19.03.17.
//  Copyright © 2017 Suvorov Startup Community. All rights reserved.
//

import UIKit

class VoyakaTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var faceImage: UIImageView!
    @IBOutlet weak var ratingStars: RatingControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
